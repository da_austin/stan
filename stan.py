import sys
import logging
import psycopg2
from door_count_speech import *
from warranty_count_speech import *
from install_spend_speech import *
from part_count_speech import *
from five_calls_speech import *
from abuse_spend_speech import *
from emergency_spend_speech import *
from longest_service_speech import *
from check_astea import astea_check_speech
from send_email_conf import email_speech

# databas connection
dbname = rds_connect.dbname
user = rds_connect.user
host = rds_connect.host
password = rds_connect.password

logger = logging.getLogger()
logger.setLevel(logging.INFO)

try:
    conn = psycopg2.connect(dbname = dbname, user = user, host = host, password = password)
except:
    print("I am unable to connect to the database")
    sys.exit()

cur = conn.cursor()

logger.info("SUCCESS: Connection to RDS mysql instance succeeded")


#----dictionary of action functions
action_dict = {'return_door_count': door_count_speech, 'return_warranty_count': warranty_count_speech
               , 'return_install_spend': install_spend_speech, 'return_part_count': part_count_speech
               , 'return_stores_5calls': five_calls_speech, 'return_abuse_spend': abuse_spend_speech
               , 'check_astea': astea_check_speech, 'confirmation_email': email_speech
               , 'return_emergency_spend': emergency_spend_speech, 'return_longest_service': longest_service_speech}


#------function called by lambda
def handler(event, context):
    """
    function to return desired info given intent
    """

    action = get_action(event)
    speech = action_dict[action](event, cur)


    return {
        "speech": speech,
        "displayText": speech,
        "source": "apiai-stan-webhook-sample"
    }

