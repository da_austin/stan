from helper_functions import *
import rds_connect

def abuse_spend_speech(event, cur):
    start_date, end_date = get_date_period(event)
    state_name = get_state(event)
    state_cd = get_state_abbrev(state_name)
    action = get_action(event)
    query = rds_connect.query_dict[action]
    cur.execute(query.format(state_cd, start_date, end_date))
    rows = cur.fetchall()
    if (len(rows) == 0):
        return 'No store was found'
    city = rows[0][0]
    store = rows[0][1]
    abuse_spend = round(rows[0][2], 2)
    if state_name == "":
        speech = 'Store with most spending for abuse calls between {} and {} was {} in {} with ${:,} for the US'.format(start_date, end_date, store, city, abuse_spend)
    else:
        speech = 'Store with most spending for abuse calls between {} and {} was {} in {} with ${:,} for {}'.format(start_date, end_date, store, city, abuse_spend, state_name)
    return speech