import us_state_abbrev

#-----helper fucntions
def get_result(event):
    result = event.get('result')
    return result

def get_params(event):
    result = get_result(event)
    parameters = result.get('parameters')
    return parameters

def get_state(event):
    parameters = get_params(event)
    state_name = parameters.get('geo-state-us')
    return state_name

def get_state_abbrev(state_name):
    if state_name == "":
        state_cd = '%'
    else:
        state_cd = us_state_abbrev.us_state_abbrev[state_name]
    return state_cd

def get_action(event):
    result = get_result(event)
    action = result.get('action')
    return action

def get_date_period(event):
    parameters = get_params(event)
    date_period = parameters.get('date-period')
    if type(date_period) == list:
        date_period = date_period[0]
    start_date = date_period[:10]
    end_date = date_period[11:]
    return start_date, end_date

def get_part_category(event):
    parameters = get_params(event)
    part_cat = parameters.get('parts_category')
    if type(part_cat) == list:
        part_cat = part_cat[0]
    return part_cat

def get_contexts(event):
    result = get_result(event)
    contexts = result.get("contexts")
    return contexts

def get_context_parameters(event, context_name):
    contexts = get_contexts(event)
    for i in contexts:
        name = i.get("name")
        if name == context_name:
            parameters = i.get("parameters")
    return parameters

def get_context_param(event, context_name, param_name):
    parameters = get_context_parameters(event, context_name)
    email = parameters.get(param_name)
    return email