# rds connection info and queries
# demo
dbname = 'smartdoor_dev'
user ='datastage_user_dev'
host ='sbd-nexus.cqf2t29nf7qf.us-east-2.rds.amazonaws.com'
password = 'dsdev123!'

return_door_count_query = """SELECT count(distinct EQUIP_NBR)
FROM DMT_MAS_EQUIP_STORE_DIM e
INNER JOIN DMT_MAS_CUST_DIM c
ON e.SHIPTO_CUST_ID = c.CUST_ID
where EQUIP_NBR != '' AND DATE_TRUNC('year', BEGIN_WRNTY_DTE) BETWEEN '1990-01-01' AND '2100-01-01' AND
CUST_LVL_2_NBR IN ('0080000268') AND
SHIPTO_DESC NOT LIKE '%USE%' AND SHIPTO_DESC NOT LIKE '%DNU%' AND 
SHIPTO_DESC NOT LIKE '%CLOSED%' AND SHIPTO_DESC NOT LIKE '%use%' AND
SHIPTO_DESC NOT LIKE '%RELOCATED%' AND SHIPTO_DESC NOT LIKE '%OLD%'
AND ACTV_STAT_FLAG != 'N'
AND STATE_CD like '{}'"""

return_warranty_count_query = """SELECT COUNT(DISTINCT EQUIP_NBR)
FROM DMT_MAS_EQUIP_STORE_DIM e
INNER JOIN DMT_MAS_CUST_DIM c
ON e.SHIPTO_CUST_ID = c.CUST_ID
where EQUIP_NBR != '' AND BEGIN_WRNTY_DTE BETWEEN '1990-01-01' AND '2100-01-01' 
AND END_WRNTY_DTE BETWEEN '1990-01-01' AND '2100-01-01'
AND CUST_LVL_2_NBR IN ('0080000268') AND
SHIPTO_DESC NOT LIKE '%USE%' AND SHIPTO_DESC NOT LIKE '%DNU%' AND 
SHIPTO_DESC NOT LIKE '%CLOSED%' AND SHIPTO_DESC NOT LIKE '%use%' AND
SHIPTO_DESC NOT LIKE '%RELOCATED%' AND SHIPTO_DESC NOT LIKE '%OLD%'
AND ACTV_STAT_FLAG != 'N'
AND STATE_CD like '{}'
AND END_WRNTY_DTE > NOW()"""

return_install_spend_query = """SELECT sum(NET_AMT_BCRNCY)
FROM DMT_MAS_EQUIP_ALLOC_INVC_FACT i
INNER JOIN DMT_MAS_EQUIP_STORE_DIM e
ON i.EQUIP_STORE_ID = e.EQUIP_STORE_ID
INNER JOIN DMT_MAS_CUST_DIM c
ON i.SHIPTO_CUST_ID = c.CUST_ID
WHERE i.EQUIP_STORE_ID != -1 AND CUST_LVL_2_NBR IN ('0080000268')
	AND age(now(), INSTALL_DTM) <= interval '3 years'
    AND SAP_SLS_ORG_NAME = 'Stanley US'
    AND STATE_CD LIKE '{}'
    AND INSTALL_DTM BETWEEN '{}' AND '{}'"""

return_part_count_query = """select count(*)
FROM DMT_MAS_SVC_ORD_FACT a 
  INNER JOIN DMT_MAS_SVC_ACTVTY_FACT b on a.SVC_ORD_ID=b.SVC_ORD_ID
  INNER JOIN DMT_MAS_PROD_DIM d on b.PROD_ID=d.PROD_ID 
  INNER JOIN DMT_MAS_CUST_DIM f on a.SHIPTO_CUST_ID=f.CUST_ID
WHERE CUST_LVL_2_NBR IN ('0080000268')
AND STATE_CD LIKE '{}'
AND CREATE_DTM BETWEEN '{}' AND '{}'
AND CUST_PROD_CTGY_CD = '{}'"""

# stores that had more than 5 service calls
return_stores_5calls_query = """select count(distinct cust_name) from
(select b.state_cd, b.cust_name, count(distinct a.svc_ord_nbr) num_calls
    from dmt_mas_svc_ord_fact a
    inner join dmt_mas_cust_dim b on a.shipto_cust_id = b.cust_id
    where b.SAP_SLS_ORG_NAME = 'Stanley US'
	   and b.CUST_LVL_2_NBR IN ('0080000268')
       and b.state_cd like '{}'
       and a.create_dtm between '{}' and '{}'
    group by b.state_cd, b.cust_name) q
 where num_calls >= 5"""

# store with most spending in abuse calls
return_most_abuse_spend_query = """select c.city_name
	 , c.cust_name
     , round(avg(a.std_labr_net_amt_dcrncy) + avg(a.ovtm_labr_net_amt_dcrncy) + avg(a.travel_net_amt_dcrncy) + avg(a.matl_net_amt_dcrncy) + avg(a.tax_net_amt_dcrncy) + avg(a.frt_net_amt_dcrncy),2) as cost
from dmt_mas_svc_ord_fact a
INNER JOIN DMT_MAS_SVC_ACTVTY_FACT b on a.SVC_ORD_ID=b.SVC_ORD_ID
inner join dmt_mas_cust_dim c on a.shipto_cust_id = c.cust_id
where c.SAP_SLS_ORG_NAME = 'Stanley US'
   and c.CUST_LVL_2_NBR IN ('0080000268')
   and c.state_cd like '{}'
   and a.create_dtm between '{}' and '{}'
   and lower(b.cause_desc) = 'abuse'
group by c.city_name, c.cust_name
order by cost desc
limit 1"""

# Total spend of emergency calls
return_emergency_spend_query = """select sum(std_labr_net_amt_dcrncy + ovtm_labr_net_amt_dcrncy + travel_net_amt_dcrncy + matl_net_amt_dcrncy + tax_net_amt_dcrncy + frt_net_amt_dcrncy)
    from dmt_mas_svc_ord_fact a
    inner join dmt_mas_cust_dim b on a.shipto_cust_id = b.cust_id
    inner join DMT_MAS_SVC_ORD_MISC_DIM h on a.SVC_ORD_MISC_DIM_ID = h.SVC_ORD_MISC_DIM_ID
    where b.sap_sls_org_name = 'Stanley US'
       and b.CUST_LVL_2_NBR IN ('0080000268')
	   and h.priority_desc in ('EMERGENCY-(4HR)', 'EMERGENCY (4 Hrs)')
	   and b.state_cd like '{}'
       and a.create_dtm between '{}' and '{}'"""

# stores has longest unresolved ticket
return_longest_service_query = """select b.city_name, b.cust_name,
       date_part('day', (select case when d.close_dtm is null then now() else d.close_dtm end close_ts) - a.create_dtm) :: integer as days_open
    from dmt_mas_svc_ord_fact a
    inner join dmt_mas_cust_dim b on a.shipto_cust_id = b.cust_id
    inner join dmt_mas_svc_ord_misc_dim c on a.svc_ord_misc_dim_id = c.svc_ord_misc_dim_id
    inner join dmt_mas_svc_ord_degen_dim d on a.svc_ord_id = d.svc_ord_id
    inner join dmt_mas_brnch_dim e on a.brnch_id = e.brnch_id
    where b.sap_sls_org_name = 'Stanley US'
        and b.CUST_LVL_2_NBR IN ('0080000268')
        and e.brnch_cd <> '6090'
        and b.state_cd like '{}'
        and a.create_dtm between '{}' and '{}'
        and c.priority_cd <> '5'
   order by days_open desc
   limit 1"""

# action name -> query name mapping
query_dict = {'return_door_count': return_door_count_query, 'return_warranty_count': return_warranty_count_query
              , 'return_install_spend': return_install_spend_query, 'return_part_count': return_part_count_query
              , 'return_stores_5calls': return_stores_5calls_query, 'return_abuse_spend': return_most_abuse_spend_query
              , 'return_emergency_spend': return_emergency_spend_query, 'return_longest_service': return_longest_service_query}
