from helper_functions import *
import rds_connect

def part_count_speech(event, cur):
    start_date, end_date = get_date_period(event)
    print(start_date, end_date)
    state_name = get_state(event)
    state_cd = get_state_abbrev(state_name)
    part_cat = get_part_category(event)
    print(type(part_cat))
    action = get_action(event)
    query = rds_connect.query_dict[action]
    cur.execute(query.format(state_cd, start_date, end_date, part_cat))
    rows = cur.fetchall()
    part_count = rows[0][0]
    if state_name == "":
        speech = '{} {} have been replaced in the US between {} and {}'.format(part_count, part_cat, start_date, end_date)
    else:
        speech = '{} {} have been replace in {} between {} and {}'.format(part_count, part_cat, state_name, start_date, end_date)
    return speech