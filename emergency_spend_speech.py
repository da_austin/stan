from helper_functions import *
import rds_connect

def emergency_spend_speech(event, cur):
    start_date, end_date = get_date_period(event)
    state_name = get_state(event)
    state_cd = get_state_abbrev(state_name)
    action = get_action(event)
    query = rds_connect.query_dict[action]
    cur.execute(query.format(state_cd, start_date, end_date))
    rows = cur.fetchall()
    emergency_spend = rows[0][0]
    if emergency_spend == None:
        emergency_spend = 0.00
    else:
        emergency_spend = round(emergency_spend, 2)
    if state_name == "":
        speech = 'Emergency spend between {} and {} was ${:,} for the US'.format(start_date, end_date, emergency_spend)
    else:
        speech = 'Emergency spend between {} and {} was ${:,} for {}'.format(start_date, end_date, emergency_spend, state_name)
    return speech