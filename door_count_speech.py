from helper_functions import *
import rds_connect

def door_count_speech(event, cur):
    state_name = get_state(event)
    state_cd = get_state_abbrev(state_name)
    action = get_action(event)
    query = rds_connect.query_dict[action]
    cur.execute(query.format(state_cd))
    rows = cur.fetchall()
    num_doors = rows[0][0]
    if state_name == "":
        speech = 'Ther are {} doors in the US'.format(num_doors)
    else:
        speech = 'There are {} doors in {}'.format(num_doors, state_name)
    return speech