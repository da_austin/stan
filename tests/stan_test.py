import unittest
import json
import stan

@unittest.skip("classing skipping")
class TestStoreCalls(unittest.TestCase):

	def test(self):
		event = json.load(open('./json/store_calls.json', encoding='utf8'))
		speech = stan.handler(event, 3)
		print(speech)

@unittest.skip("classing skipping")
class TestAbuseSpend(unittest.TestCase):
	def test(self):
		event = json.load(open('./json/abuse_spend.json', encoding='utf8'))
		speech = stan.handler(event, 0)
		print(speech)

@unittest.skip("classing skipping")
class TestEmergencySpend(unittest.TestCase):
	def test(self):
		event = json.load(open('./json/emergency.json', encoding='utf8'))
		speech = stan.handler(event, 0)
		print(speech)

#@unittest.skip("classing skipping")
class TestLongestService(unittest.TestCase):
	def test(self):
		event = json.load(open('./json/longest_service.json', encoding='utf8'))
		speech = stan.handler(event, 0)
		print(speech)

if __name__ == '__main__':
    unittest.main()