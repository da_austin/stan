import boto3
import logging
from botocore.exceptions import ClientError
from helper_functions import *
# from . import settings

logger = logging.getLogger(__name__)

def send_email(recipient, subject, html):

    # Specify a configuration set. If you do not want to use a configuration
    # set, comment the following variable, and the
    # ConfigurationSetName=CONFIGURATION_SET argument below.
    # CONFIGURATION_SET = "ConfigSet"

    AWS_REGION = "us-east-1"

    CHARSET = "UTF-8"

    sender = 'S-SAT-VirtualAssistant@sbdinc.com'

    # Create a new SES resource and specify a region.
    client = boto3.client('ses', region_name=AWS_REGION)
    try:
        response = client.send_email(
            Destination={
                'ToAddresses': [
                    recipient,
                ],
            },
            Message={
                'Body': {
                    'Html': {
                        'Charset': CHARSET,
                        'Data': html,
                    },
                },
                'Subject': {
                    'Charset': CHARSET,
                    'Data': subject,
                },
            },
            Source=sender,
            # If you are not using a configuration set, comment or delete the
            # following line
            # ConfigurationSetName=CONFIGURATION_SET,
        )
    except ClientError as e:
        logger.error(e.response['Error']['Message'] + ' ' + recipient)
        return 'F', e.response['Error']['Message']
    else:
        logger.info("Email sent! Message ID:" + response['ResponseMetadata']['RequestId'] + ' to ' + recipient)
        return 'S', response['ResponseMetadata']['RequestId']


def email_speech(event, cur):

    recipient = get_context_param(event, 'service_ticket', 'email').lower()
    contact_name = get_context_param(event, 'service_ticket', 'name')
    company_name = get_context_param(event, 'service_ticket', 'company_name')
    address = get_context_param(event, 'service_ticket', 'address')

    subject = 'Service Ticket Confirmation'
    html = 'A service ticket has be created for {} at {}. If neccessary we will contact {} regarding this ' \
           'service ticket'.format(company_name, address, contact_name)

    send_email(recipient, subject, html)

    speech = 'We have sent a confirmation email to {}'.format(contact_name)
    return speech