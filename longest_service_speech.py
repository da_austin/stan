from helper_functions import *
import rds_connect

def longest_service_speech(event, cur):
    start_date, end_date = get_date_period(event)
    state_name = get_state(event)
    state_cd = get_state_abbrev(state_name)
    action = get_action(event)
    query = rds_connect.query_dict[action]
    cur.execute(query.format(state_cd, start_date, end_date))
    rows = cur.fetchall()
    city = rows[0][0]
    store = rows[0][1]
    days_open = rows[0][2]
    if state_name == "":
        speech = 'Store with longest open ticket between {} and {} was {} in {} with {} days open for the US'.format(start_date, end_date, store, city, days_open)
    else:
        speech = 'Store with longest open ticket between {} and {} was {} in {} with {} days open for {}'.format(start_date, end_date, store, city, days_open, state_name)
    return speech