from helper_functions import *

list_emails = ['lee.saeugling@sbdinc.com', 'dian.jiao@sbdinc.com', 'stanleyinsights@gmail.com',
               'Lee.Saeugling@sbdinc.com', 'Dian.jiao@sbdinc.com']

def astea_check_speech(event, cur):
    email = get_context_param(event, 'service_ticket', 'email')
    name = get_context_param(event, 'service_ticket', 'name')
    if email in list_emails:
        speech = "Thank you we have {}'s contact information in our system. What is " \
                 "the name of your company?".format(name)
    else:
        speech = "We did not have {}'s contact information in our system but I have added it. " \
                 "What is the name of your company?".format(name)
    return speech