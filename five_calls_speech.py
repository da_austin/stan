from helper_functions import *
import rds_connect

def five_calls_speech(event, cur):
    start_date, end_date = get_date_period(event)
    state_name = get_state(event)
    state_cd = get_state_abbrev(state_name)
    action = get_action(event)
    query = rds_connect.query_dict[action]
    cur.execute(query.format(state_cd, start_date, end_date))
    rows = cur.fetchall()
    stores = rows[0][0]
    if stores == None:
        stores = 0

    if state_name == "":
        speech = 'There are {} stores between {} and {} with more than 5 service calls for the US'.format(stores, start_date, end_date)
    else:
        speech = 'There are {} stores between {} and {} with more than 5 service calls for {}'.format(stores, start_date, end_date, state_name)
    return speech